# Todo List App
## Overview
Simple app for keeping track of todo lists per user. This app was built using the following
technologies
- Kotlin
- Spring Boot
- Hibernate

The app runs on top of a H2 in memory db

## Launch the App
Open a terminal and navigate to the projects root directory and run the following
```
mvn spring-boot:run
```

## Users
#### User 1
- username: user1
- password: password1
#### User 2
- username: user2
- password: password2

## APIs
The app can be reached on
> localhost:8080

### Todo List REST endpoints

The following APIs all require a user to be logged in and will prevent users from updating resources they do not own

```
GET  /todos: returns all todo lists for a given user
POST /todos: creates a new todo list for a given user
PUT /todos/{id}: updates a todo list
DELETE /todos/{id}: deletes a todo list

POST /todos/{todoListId}/tasks: creates a new task under a given todo list
PUT /todos/{todoListId}/tasks/{id}: updates a given task
DELETE /todos/{todoListId}/tasks/{id}: removes a given task
 ```

An Insomnia.json file is included for testing purposes. 
File requires Insomnia REST Client to be installed on the users machine. 
Insomnia can be downloaded [here](https://insomnia.rest/)

## Improvements
- Add swagger endpoint
- Add APIs for creating users
- Improve security on login
- Exceptions handler returning proper error messages
- liquibase / flyway for db migration
- Unit and Integration tests
- Additional features to enrich the application
- A UI
