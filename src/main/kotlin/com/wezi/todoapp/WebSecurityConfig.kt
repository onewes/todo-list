package com.wezi.todoapp

import com.wezi.todoapp.user.UserInfoService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.InMemoryUserDetailsManager

@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
            .cors().and().csrf().disable()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/", "/test").permitAll()
            .anyRequest().authenticated()
            .and()
            .httpBasic()
            .and()
            .logout()
            .permitAll()
    }

    @Bean
    public override fun userDetailsService(): UserDetailsService {
        val userInfoService = this.applicationContext.getBean(UserInfoService::class.java)
        val users = userInfoService.fetchAllUsers().map {
            User.withDefaultPasswordEncoder()
                .username(it.username)
                .password(it.password)
                .roles("ROLE")
                .build()
        }
        return InMemoryUserDetailsManager(users)
    }
}