package com.wezi.todoapp.controllers

import com.wezi.todoapp.todo.base.TodoListDto
import com.wezi.todoapp.todo.base.TodoListService
import com.wezi.todoapp.todo.task.TodoTaskDto
import com.wezi.todoapp.todo.task.TodoTaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/todos")
class TodoListController @Autowired constructor(
    private val todoListService: TodoListService,
    private val todoTaskService: TodoTaskService
) {

    @GetMapping
    fun fetchAllTodos() = todoListService.fetchAll()

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createTodo(@RequestBody todoList: TodoListDto) = todoListService.create(todoList)

    @PutMapping("/{id}")
    fun updateTodo(@RequestBody todoList: TodoListDto) = todoListService.update(todoList)

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteTodo(@PathVariable id: Long) = todoListService.delete(id)

    @PostMapping("/{todoListId}/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    fun createTask(@PathVariable todoListId: Long, @RequestBody todoTask: TodoTaskDto): TodoTaskDto {
        todoTask.todoListId = todoListId
        return todoTaskService.create(todoTask)
    }

    @PutMapping("/{todoListId}/tasks/{id}")
    fun updateTsk(@PathVariable todoListId: Long, @RequestBody todoTask: TodoTaskDto): TodoTaskDto {
        todoTask.todoListId = todoListId
        return todoTaskService.update(todoTask)
    }

    @DeleteMapping("/{todoListId}/tasks/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteTask(@PathVariable id: Long) = todoTaskService.delete(id)
}