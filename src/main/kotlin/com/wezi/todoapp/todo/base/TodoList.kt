package com.wezi.todoapp.todo.base

import com.wezi.todoapp.todo.task.TodoTask
import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany

@Entity
data class TodoList(
    @Id
    @Column(insertable = false, updatable = false)
    @GenericGenerator(name = "native", strategy = "native")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    var id: Long = 0,
    var name: String = "",
    @Column(name = "user_info_id")
    var userInfoId: Long,
    @OneToMany
    @JoinColumn(name = "todo_list_id", referencedColumnName = "id")
    var todoTasks: List<TodoTask> = emptyList()
) {

    fun toDto() = TodoListDto(id = id, name = name, todoTasks = todoTasks.map { it.toDto() })
}