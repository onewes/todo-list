package com.wezi.todoapp.todo.base

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.wezi.todoapp.todo.task.TodoTaskDto

@JsonIgnoreProperties(ignoreUnknown = true)
data class TodoListDto(var id: Long = 0, var name: String = "", var todoTasks: List<TodoTaskDto> = emptyList())