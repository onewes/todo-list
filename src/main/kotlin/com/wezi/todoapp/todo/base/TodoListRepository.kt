package com.wezi.todoapp.todo.base

import org.springframework.data.jpa.repository.JpaRepository

interface TodoListRepository : JpaRepository<TodoList, Long> {
    fun findAllByUserInfoId(userInfoId: Long): List<TodoList>
}