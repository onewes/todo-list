package com.wezi.todoapp.todo.base

interface TodoListService {
    fun fetchAll(): List<TodoListDto>
    fun fetchById(id: Long): TodoListDto
    fun create(todoList: TodoListDto): TodoListDto
    fun update(todoList: TodoListDto): TodoListDto
    fun delete(id: Long)
    fun isAuthorizedUser(id: Long): Boolean
}