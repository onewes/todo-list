package com.wezi.todoapp.todo.base

import com.wezi.todoapp.user.UserInfoService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TodoListServiceImpl @Autowired constructor(
    private val repository: TodoListRepository,
    private val userInfoService: UserInfoService
) : TodoListService {
    override fun fetchAll(): List<TodoListDto> {
        val user = userInfoService.fetchByUsername(UserInfoService.loggedInUsername())
        return repository.findAllByUserInfoId(user.id).map { it.toDto() }
    }

    @Throws(IllegalAccessException::class)
    override fun fetchById(id: Long): TodoListDto {
        val todoList = repository.findById(id)
            .orElseThrow { throw NotFoundException("No Todo List found with id=$id") }
        val user = userInfoService.fetchByUsername(UserInfoService.loggedInUsername())
        if (user.id != todoList.userInfoId) throw IllegalAccessException("User not authorized to fetch todo list")
        return todoList.toDto()
    }

    override fun create(todoList: TodoListDto): TodoListDto {
        val user = userInfoService.fetchByUsername(UserInfoService.loggedInUsername())
        val savedTodoList = repository.save(
            TodoList(
                name = todoList.name,
                userInfoId = user.id
            )
        )
        return savedTodoList.toDto()
    }

    @Throws(IllegalAccessException::class)
    override fun update(todoList: TodoListDto): TodoListDto {
        val user = userInfoService.fetchByUsername(UserInfoService.loggedInUsername())
        // todo should check if todo list exists
        val existingTodoList = repository.findById(todoList.id).get()
        if (existingTodoList.userInfoId == user.id) {
            return repository.save(
                TodoList(
                    id = todoList.id,
                    name = todoList.name,
                    userInfoId = user.id
                )
            ).toDto()
        } else throw IllegalAccessException("User not authorized to update todolist")
    }

    override fun delete(id: Long) {
        isAuthorizedUser(id)
        repository.deleteById(id)
    }

    @Throws(IllegalAccessException::class, NotFoundException::class)
    override fun isAuthorizedUser(id: Long): Boolean {
        val todoList = repository.findById(id)
            .orElseThrow { throw NotFoundException("No Todo List found with id=$id") }
        val user = userInfoService.fetchByUsername(UserInfoService.loggedInUsername())
        if (user.id != todoList.userInfoId) throw IllegalAccessException("User not authorized to fetch todo list")
        return true
    }
}