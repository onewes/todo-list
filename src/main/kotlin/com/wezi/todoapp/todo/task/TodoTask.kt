package com.wezi.todoapp.todo.task

import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class TodoTask(
    @Id
    @Column(insertable = false, updatable = false)
    @GenericGenerator(name = "native", strategy = "native")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    var id: Long = 0,
    @Column(name = "todo_list_id")
    var todoListId: Long = 0,
    var name: String = "",
    var completed: Boolean = false
) {

    fun toDto() = TodoTaskDto(id = id, name = name, completed = completed, todoListId = todoListId)
}