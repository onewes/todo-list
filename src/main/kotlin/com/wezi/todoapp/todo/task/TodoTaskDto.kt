package com.wezi.todoapp.todo.task

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class TodoTaskDto(
    var id: Long = 0,
    var todoListId: Long = 0,
    var name: String = "",
    var completed: Boolean = false
)