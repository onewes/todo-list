package com.wezi.todoapp.todo.task

import org.springframework.data.jpa.repository.JpaRepository

interface TodoTaskRepository :
    JpaRepository<TodoTask, Long> {
    fun findAllByTodoListId(todoListId: Long): List<TodoTask>
}