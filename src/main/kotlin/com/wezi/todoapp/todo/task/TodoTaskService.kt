package com.wezi.todoapp.todo.task

interface TodoTaskService {
    fun fetchAll(todoListId: Long): List<TodoTaskDto>
    fun create(todoTask: TodoTaskDto): TodoTaskDto
    fun update(todoTask: TodoTaskDto): TodoTaskDto
    fun delete(todoTaskId: Long)
}