package com.wezi.todoapp.todo.task

import com.wezi.todoapp.todo.base.TodoListService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TodoTaskServiceImpl @Autowired constructor(
    private val repository: TodoTaskRepository,
    private val todoListService: TodoListService
) : TodoTaskService {

    // todo refactor
    override fun fetchAll(todoListId: Long): List<TodoTaskDto> {
        val todoList = todoListService.fetchById(todoListId)
        return repository.findAllByTodoListId(todoListId).map { it.toDto() }
    }

    override fun create(todoTask: TodoTaskDto): TodoTaskDto {
        todoTask.id = 0
        return save(todoTask)
    }

    private fun save(todoTask: TodoTaskDto): TodoTaskDto {
        val todoList = todoListService.fetchById(todoTask.todoListId)
        val savedTodoTask =
            repository.save(
                TodoTask(
                    id = todoTask.id,
                    todoListId = todoTask.todoListId,
                    name = todoTask.name,
                    completed = todoTask.completed
                )
            )
        return savedTodoTask.toDto()
    }

    override fun update(todoTask: TodoTaskDto) = save(todoTask)

    override fun delete(todoTaskId: Long) {
        val todoTask =
            repository.findById(todoTaskId)
                .orElseThrow { throw NotFoundException("No Todo Task found with id=$todoTaskId") }
        todoListService.isAuthorizedUser(todoTask.todoListId)
        repository.deleteById(todoTaskId)
    }
}