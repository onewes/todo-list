package com.wezi.todoapp.user

import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class UserInfo(
    @Id
    @Column(insertable = false, updatable = false)
    @GenericGenerator(name = "native", strategy = "native")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    var id: Long = 0,
    var username: String = "",
    var password: String = ""
) {
    fun toDto() = UserInfoDto(id = id, username = username, password = password)
}