package com.wezi.todoapp.user

data class UserInfoDto(
    var id: Long = 0,
    var username: String = "",
    var password: String = ""
)