package com.wezi.todoapp.user

import org.springframework.data.jpa.repository.JpaRepository

interface UserInfoRepository :
    JpaRepository<UserInfo, Long> {
    fun findByUsername(username: String): UserInfo?
}