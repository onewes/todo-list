package com.wezi.todoapp.user

import org.springframework.security.access.AccessDeniedException
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails

interface UserInfoService {
    fun fetchAllUsers(): List<UserInfoDto>
    fun fetchByUsername(username: String): UserInfoDto

    companion object {
        @Throws(AccessDeniedException::class)
        fun loggedInUsername(): String {
            val authentication = SecurityContextHolder.getContext().authentication
            if (authentication !is AnonymousAuthenticationToken)
                return (authentication.principal as UserDetails).username
            throw AccessDeniedException("No logged in user found")
        }
    }
}