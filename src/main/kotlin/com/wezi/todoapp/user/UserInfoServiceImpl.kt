package com.wezi.todoapp.user

import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserInfoServiceImpl @Autowired constructor(private val repository: UserInfoRepository) :
    UserInfoService {
    override fun fetchAllUsers() = repository.findAll().map { it.toDto() }.toList()

    override fun fetchByUsername(username: String) =
        repository.findByUsername(username)?.toDto()
            ?: throw NotFoundException("No user found with username=$username")
}