create table if not exists user_info
(
    id       int(11)      not null auto_increment,
    username varchar(128) not null,
    password varchar(128) not null,
    primary key (id)
);

create table if not exists todo_list
(
    id           int(11)     not null auto_increment,
    user_info_id int(11)     not null,
    name         varchar(64) not null,
    primary key (id),
    constraint PK_todo_list_user_info_id foreign key (user_info_id) references user_info (id)
);

create table if not exists todo_task
(
    id           int(11)      not null auto_increment,
    todo_list_id int(11)      not null,
    name         varchar(256) not null,
    completed    tinyint(1) default false,
    primary key (id),
    constraint PK_todo_task_todo_list_id foreign key (todo_list_id) references todo_list (id)
);

insert into user_info(username, password)
values ('user1', 'password1'),
       ('user2', 'password2');