package com.wezi.todoapp.todo.base

import com.wezi.todoapp.todo.task.TodoTask
import com.wezi.todoapp.user.UserInfoDto
import com.wezi.todoapp.user.UserInfoService
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.FunSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import javassist.NotFoundException
import org.springframework.boot.test.context.SpringBootTest
import java.util.Optional

@SpringBootTest(classes = [TodoListServiceImpl::class])
class TodoListServiceImplTest : FunSpec({

    val USER_INFO_ID1 = 1L
    val USERNAME1 = "USER1"
    val USER_INFO_ID2 = 2L
    val USERNAME2 = "USER2"

    val TODO_ID1 = 1L
    val TODO_ID2 = 2L
    val TODO_NAME1 = "TODO1"
    val TODO_NAME2 = "TODO2"

    val TODO_TASK_ID1 = 1L
    val TODO_TASK_ID2 = 2L
    val TODO_TASK_NAME1 = "TASK1"
    val TODO_TASK_NAME2 = "TASK2"

    test("fetchAll should return a list of todos") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo1 = TodoList(
            TODO_ID1,
            TODO_NAME1,
            USER_INFO_ID1,
            listOf(TodoTask(TODO_TASK_ID1, TODO_ID1, TODO_TASK_NAME1, false))
        )
        val todo2 = TodoList(
            TODO_ID2,
            TODO_NAME2,
            USER_INFO_ID1,
            listOf(TodoTask(TODO_TASK_ID2, TODO_ID2, TODO_TASK_NAME2, false))
        )

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findAllByUserInfoId(USER_INFO_ID1) } returns listOf(todo1, todo2)

        val actualTodos = todoListService.fetchAll()

        val expectedTodo1 = todo1.toDto()
        val expectedTodo2 = todo2.toDto()

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findAllByUserInfoId" withArguments listOf(USER_INFO_ID1) }
        actualTodos.size shouldBe 2
        actualTodos[0] shouldBe expectedTodo1
        actualTodos[1] shouldBe expectedTodo2
    }

    test("fetchAll with empty list of todos for a given user") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findAllByUserInfoId(USER_INFO_ID1) } returns emptyList()

        val actualTodos = todoListService.fetchAll()

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findAllByUserInfoId" withArguments listOf(USER_INFO_ID1) }
        actualTodos.size shouldBe 0
    }

    test("fetchById with a valid todo id") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo1 = TodoList(
            TODO_ID1,
            TODO_NAME1,
            USER_INFO_ID1,
            listOf(TodoTask(TODO_TASK_ID1, TODO_ID1, TODO_TASK_NAME1, false))
        )

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo1)

        val actualTodo = todoListService.fetchById(TODO_ID1)
        val expectedTodo = todo1.toDto()

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        actualTodo shouldBe expectedTodo
    }

    test("fetchById with an invalid todo id") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID2) } returns Optional.empty()

        shouldThrow<NotFoundException> { todoListService.fetchById(TODO_ID2) }

        verify(exactly = 0) { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID2) }
    }

    test("fetchById for an unauthorized user") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo1 = TodoList(
            TODO_ID1,
            TODO_NAME1,
            USER_INFO_ID1,
            listOf(TodoTask(TODO_TASK_ID1, TODO_ID1, TODO_TASK_NAME1, false))
        )

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME2

        val user = UserInfoDto(USER_INFO_ID2, USERNAME2, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo1)

        shouldThrow<IllegalAccessException> { todoListService.fetchById(TODO_ID1) }

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME2) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
    }

    test("create a new todo list") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todoDto = TodoListDto(name = TODO_NAME1)
        val todo = TodoList(name = TODO_NAME1, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.save(todo) } returns todo

        val actualTodo = todoListService.create(todoDto)

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        actualTodo shouldBe todoDto
    }

    test("update an existing todo with an authorized user") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)

        // name should be updated from name1 to name2
        val todoDto = TodoListDto(id = TODO_ID1, name = TODO_NAME2)
        val todo = TodoList(id = TODO_ID1, name = TODO_NAME1, userInfoId = USER_INFO_ID1)
        val updatedTodo = TodoList(id = TODO_ID1, name = TODO_NAME2, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo)
        every { repository.save(updatedTodo) } returns updatedTodo

        val actualTodo = todoListService.update(todoDto)

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        actualTodo shouldBe todoDto
    }

    test("update an existing todo with an unauthorized user") {
        val repository = mockk<TodoListRepository>()
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todoDto = TodoListDto(id = TODO_ID1, name = TODO_NAME2)
        val todo = TodoList(id = TODO_ID1, name = TODO_NAME1, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME2

        val user = UserInfoDto(USER_INFO_ID2, USERNAME2, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo)

        shouldThrow<IllegalAccessException> { todoListService.update(todoDto) }

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME2) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
    }

    test("delete with a valid id for an authorized user") {
        val repository = mockk<TodoListRepository>(relaxed = true)
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo = TodoList(id = TODO_ID1, name = TODO_NAME1, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo)

        todoListService.delete(TODO_ID1)

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        verify { repository invoke "deleteById" withArguments listOf(TODO_ID1) }
    }

    test("delete with a valid id for an unauthorized user") {
        val repository = mockk<TodoListRepository>(relaxed = true)
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo = TodoList(id = TODO_ID1, name = TODO_NAME1, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME2

        val user = UserInfoDto(USER_INFO_ID2, USERNAME2, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo)

        shouldThrow<IllegalAccessException> { todoListService.delete(TODO_ID1) }

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME2) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        verify(exactly = 0) { repository invoke "deleteById" withArguments listOf(TODO_ID1) }
    }

    test("delete with an invalid id for an unauthorized user") {
        val repository = mockk<TodoListRepository>(relaxed = true)
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME2

        val user = UserInfoDto(USER_INFO_ID2, USERNAME2, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.empty()

        shouldThrow<NotFoundException> { todoListService.delete(TODO_ID1) }

        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        verify(exactly = 0) { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME2) }
        verify(exactly = 0) { repository invoke "deleteById" withArguments listOf(TODO_ID1) }
    }

    test("isAuthorized with a valid id for an authorized user") {
        val repository = mockk<TodoListRepository>(relaxed = true)
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo = TodoList(id = TODO_ID1, name = TODO_NAME1, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME1

        val user = UserInfoDto(USER_INFO_ID1, USERNAME1, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo)

        val actualResult = todoListService.isAuthorizedUser(TODO_ID1)

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME1) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        actualResult shouldBe true
    }

    test("isAuthorized with a valid id for an unauthorized user") {
        val repository = mockk<TodoListRepository>(relaxed = true)
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)
        val todo = TodoList(id = TODO_ID1, name = TODO_NAME1, userInfoId = USER_INFO_ID1)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME2

        val user = UserInfoDto(USER_INFO_ID2, USERNAME2, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.of(todo)

        shouldThrow<IllegalAccessException> { todoListService.isAuthorizedUser(TODO_ID1) }

        verify { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME2) }
        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
    }

    test("isAuthorized with an invalid id for an unauthorized user") {
        val repository = mockk<TodoListRepository>(relaxed = true)
        val userInfoService = mockk<UserInfoService>()
        val todoListService = TodoListServiceImpl(repository, userInfoService)

        mockkObject(UserInfoService)
        every { UserInfoService.loggedInUsername() } returns USERNAME2

        val user = UserInfoDto(USER_INFO_ID2, USERNAME2, "")
        every { userInfoService.fetchByUsername(any()) } returns user
        every { repository.findById(TODO_ID1) } returns Optional.empty()

        shouldThrow<NotFoundException> { todoListService.isAuthorizedUser(TODO_ID1) }

        verify { repository invoke "findById" withArguments listOf(TODO_ID1) }
        verify(exactly = 0) { userInfoService invoke "fetchByUsername" withArguments listOf(USERNAME2) }
    }
})